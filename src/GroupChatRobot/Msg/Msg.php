<?php

namespace WxWorkSDK\GroupChatRobot\Msg;

use WxWorkSDK\Http\Http;

/**
 * Class Msg
 * @package WxWorkSDK\GroupChatRobot\Msg
 */
abstract class Msg
{
    private $robotKey = '';

    /**
     * Msg constructor.
     * @param  string  $robotKey
     */
    public function __construct(string $robotKey)
    {
        $this->robotKey = $robotKey;
    }

    /**
     * 导出消息内容
     * @return array
     */
    public abstract function msgBody(): array;

    /**
     * @return array
     */
    public function send(): array
    {
        return Http::request('post', 'webhook/send?key='.$this->robotKey, [
            'json' => $this->msgBody(),
        ]);
    }
}