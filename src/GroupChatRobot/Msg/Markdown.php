<?php

namespace WxWorkSDK\GroupChatRobot\Msg;


/**
 * Class Text
 * @package WxWorkSDK\GroupChatRobot\Types
 */
class Markdown extends Msg
{
    private $msgType = 'markdown';
    private $content = '';

    /**
     * @param  string  $content
     * @return Markdown
     */
    public function setContent(string $content): Markdown
    {
        $this->content = substr($content, 0, 4096);
        return $this;
    }

    /**
     * @return string
     */
    public function msgBody(): array
    {
        return [
            'msgtype'  => $this->msgType,
            'markdown' => [
                'content' => $this->content
            ],
        ];
    }
}