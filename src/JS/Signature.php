<?php

namespace WxWorkSDK\JS;

/**
 * Class Signature
 * @package WxWorkSDK\JS
 */
class Signature
{
    /**
     * 获得随机字符串
     * @param  int  $len  需要的长度
     * @param  bool  $special  是否需要特殊符号
     * @return string       返回随机字符串
     */
    public static function getRandomStr($len = 32, $special = false)
    {
        $chars = [
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "m",
            "n",
            "o",
            "p",
            "q",
            "r",
            "s",
            "t",
            "u",
            "v",
            "w",
            "x",
            "y",
            "z",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z",
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"
        ];

        if ($special) {
            $chars = array_merge($chars, [
                "!",
                "@",
                "#",
                "$",
                "?",
                "|",
                "{",
                "/",
                ":",
                ";",
                "%",
                "^",
                "&",
                "*",
                "(",
                ")",
                "-",
                "_",
                "[",
                "]",
                "}",
                "<",
                ">",
                "~",
                "+",
                "=",
                ",",
                "."
            ]);
        }

        $charsLen = count($chars) - 1;
        shuffle($chars);                            //打乱数组顺序
        $str = '';
        for ($i = 0; $i < $len; $i++) {
            $str .= $chars[mt_rand(0, $charsLen)];    //随机取出一位
        }
        return $str;
    }

    /**
     * @param  string  $jsApiTicket
     * @param  string  $url
     * @return array
     */
    public static function sign(string $jsApiTicket, string $url): array
    {
        $params = [
            'jsapi_ticket' => $jsApiTicket,
            'noncestr'     => self::getRandomStr(),
            'timestamp'    => time(),
            'url'          => $url,
        ];
        $str = '';
        foreach ($params as $param => $paramValue) {
            $str .= $param.'='.$paramValue.'&';
        }
        $str = substr($str, 0, -1);
        $params['signature'] = sha1($str);
        return $params;
    }
}