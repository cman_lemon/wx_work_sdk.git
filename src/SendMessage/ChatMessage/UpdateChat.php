<?php

namespace WxWorkSDK\SendMessage\ChatMessage;

use WxWorkSDK\Error\Error;
use WxWorkSDK\Http\Http;
use WxWorkSDK\Result;

class UpdateChat
{
    const URI = 'appchat/update?access_token=';
    /**
     * @var string
     * @annotation 调用接口凭证
     */
    public $accessToken = '';
    /**
     * @var string
     * @annotation 群聊id
     */
    public $chatId = '';
    /**
     * @var string
     * @annotation 新的群聊名。若不需更新，请忽略此参数。最多50个utf8字符，超过将截断
     */
    public $name = '';
    /**
     * @var string
     * @annotation 新群主的id。若不需更新，请忽略此参数
     */
    public $owner = '';
    /**
     * @var string
     * @annotation 添加成员的id列表
     */
    public $addUserList = [];
    /**
     * @var string
     * @annotation 踢出成员的id列表
     */
    public $delUserList = [];

    /**
     * UpdateChat constructor.
     * @param string $accessToken
     * @param string $chatId
     * @param string $name
     * @param string $owner
     * @param array  $addUserList
     * @param array  $delUserList
     */
    public function __construct(string $accessToken, string $chatId, string $name = '', string $owner = '', array $addUserList = [], array $delUserList = [])
    {
        $this->accessToken = $accessToken;
        $this->chatId      = $chatId;
        $this->name        = $name;
        $this->owner       = $owner;
        $this->addUserList = $addUserList;
        $this->delUserList = $delUserList;
    }

    /**
     * @return Result
     * @annotation
     */
    public function update(): Result
    {
        if ($check = $this->check() != 0) {
            return new Result(FALSE, 1000, $check);
        }
        $postData = [
            'chatid'       => $this->chatId,
            'access_token' => $this->accessToken
        ];
        if (!empty($this->name)) {
            $postData['name'] = $this->name;
        }
        if (!empty($this->owner)) {
            $postData['owner'] = $this->owner;
        }
        if (!empty($this->addUserList)) {
            $postData['add_user_list'] = $this->addUserList;
        }
        if (!empty($this->delUserList)) {
            $postData['del_user_list'] = $this->delUserList;
        }
        list($bool, $response) = Http::request('POST', self::URI . $this->accessToken, ["json" => $postData]);
        if ($bool) {
            return $this->respond($response);
        } else {
            return $response;
        }
    }

    /**
     * @return int|string
     * @annotation
     */
    private function check()
    {
        if (empty($this->token)) {
            return '无access_token';
        }
        if (empty($this->chatId)) {
            return '无群聊id';
        }
        if (empty($this->name) && empty($this->owner) && empty($this->addUserList) && empty($this->delUserList)) {
            return '无更新资料';
        }
        return 0;
    }

    /**
     * @param $response
     * @return Result
     * @annotation
     */
    private function respond($response): Result
    {
        $response = json_decode($response, TRUE);
        if ($response['errcode'] == 0) {
            return new Result(TRUE, 0, 'ok');
        } else {
            return new Result(FALSE, Error::WX_MISTAKE, $response['errcode'] . ':' . $response['errmsg']);
        }
    }
}