<?php

namespace WxWorkSDK\SendMessage\ApplicationMessage;

/**
 * Class Markdown
 * @package WxWorkSDK\SendMessage\ApplicationMessage
 */
class Markdown extends SendBase
{
    /**
     * @var string
     * @annotation 标题，不超过128个字节，超过会自动截断
     */
    private $content;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     * @annotation
     */
    public function buildParam(): array
    {
        $postData = [
            "msgtype"                  => "markdown",
            "agentid"                  => $this->agentId,
            "markdown"                 => [
                "content" => $this->content,
            ],
            'enable_duplicate_check'   => $this->enable_duplicate_check,
            'duplicate_check_interval' => $this->duplicate_check_interval,
        ];
        if ($this->toUser) {
            $postData['touser'] = $this->toUser;
        }
        if ($this->toParty) {
            $postData['toparty'] = $this->toParty;
        }
        if ($this->toParty) {
            $postData['totag'] = $this->toTag;
        }
        return $postData;
    }

    /**
     * @return string
     * @annotation 按照规则自定义人数据检测
     */
    public function customCheck(): string
    {
        if (empty($this->content)) {
            return "消息内容为空";
        }
        return '';
    }
}