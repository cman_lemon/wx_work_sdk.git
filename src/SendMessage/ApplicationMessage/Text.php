<?php

namespace WxWorkSDK\SendMessage\ApplicationMessage;

/**
 * Class Text
 * @package  WxWorkSDK\SendMessage\ApplicationMessage
 * @author   George
 * @datetime 2019/5/27 10:15
 * @annotation
 */
class Text extends SendBase
{
    private $content = '';

    /**
     * Text constructor.
     * @param  string  $content
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     * @annotation
     */
    public function buildParam(): array
    {
        $postData = [
            "msgtype" => "text",
            "agentid" => $this->agentId,
            "safe"    => $this->safe,
            "text"    => ["content" => $this->content]
        ];
        if ($this->toUser) {
            $postData['touser'] = $this->toUser;
        }
        if ($this->toParty) {
            $postData['toparty'] = $this->toParty;
        }
        if ($this->toParty) {
            $postData['totag'] = $this->toTag;
        }
        return $postData;
    }

    /**
     * @return string
     * @annotation
     */
    public function customCheck(): string
    {
        if (empty($this->content)) {
            return "无消息内容";
        }
        return '';
    }
}