<?php

namespace WxWorkSDK;

/**
 * Class Result
 * @package  WxWorkSDK
 * @author   George
 * @datetime 2019/5/25 11:27
 * @annotation
 */
class Result
{
    private $state = TRUE;

    private $code = 0;

    private $msg = "";

    private $result = [];

    public function __construct(bool $state, $code = 0, $msg = "", $result = [])
    {
        $this->state  = $state;
        $this->code   = $code;
        $this->msg    = $msg;
        $this->result = $result;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getMsg()
    {
        return $this->msg;
    }

    public function getContent()
    {
        return $this->result;
    }
}